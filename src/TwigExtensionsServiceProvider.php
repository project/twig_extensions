<?php

namespace Drupal\twig_extensions;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Registers the twig services.
 */
class TwigExtensionsServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    parent::register($container);

    // Ensure the Intl PHP extension is available before adding the service.
    if (class_exists('IntlDateFormatter') && class_exists('Twig_Extensions_Extension_Intl')) {
      $container->register('twig_extensions.twig.intl', '\Twig_Extensions_Extension_Intl')
        ->addTag('twig.extension');
    }

    // Ensure the other extension classes are available before adding the services.
    if (class_exists('Twig_Extensions_Extension_Array')) {
      $container->register('twig_extensions.twig.array', '\Twig_Extensions_Extension_Array')
        ->addTag('twig.extension');
    }
    if (class_exists('Twig_Extensions_Extension_Date')) {
      $container->register('twig_extensions.twig.date', '\Twig_Extensions_Extension_Date')
        ->addTag('twig.extension');
    }
    if (class_exists('Twig_Extensions_Extension_Text')) {
      $container->register('twig_extensions.twig.text', '\Twig_Extensions_Extension_Text')
        ->addTag('twig.extension');
    }

  }

}
